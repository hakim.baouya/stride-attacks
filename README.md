# STRIDE-Attacks




## Description
The project include two strides attacks models: tampering and spoofing that harms software architecure. The connectors include all the malicious actions.


## Installation
Models are written in PRISM language that supports probabilistic automata in Markov Decision Process. The query language relys on PCTL to check the presence of attacks on the architecure.

## Usage
The usage of these models necissitates the authors confirmation.

## Project status
The project is in progress to support more attacks.
